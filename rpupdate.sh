#!/bin/bash

LINUX_UPDATE="linux_update/rockdev/Image/"
if [ ! -d $LINUX_UPDATE ]
        then
        {
                cd linux_update/rockdev/
                mkdir Image
                chmod -R 777 Image
                cd -
        }
fi
BOARD_DIR="rockdev/Image-rk3399_mid"
cp u-boot/rk3399_loader_v1.08.109.bin linux_update/rockdev/MiniLoaderAll.bin
cp u-boot/rk3399_loader_v1.12.109.bin linux_update/rockdev/MiniLoaderAll.bin
cp u-boot/trust.img 				$LINUX_UPDATE
cp u-boot/uboot.img 				$LINUX_UPDATE
cp kernel/resource.img 				$LINUX_UPDATE
cp kernel/kernel.img 				$LINUX_UPDATE
cp $BOARD_DIR/misc.img 				$LINUX_UPDATE
cp $BOARD_DIR/boot.img 				$LINUX_UPDATE
cp $BOARD_DIR/recovery.img 			$LINUX_UPDATE
cp $BOARD_DIR/system.img 			$LINUX_UPDATE
cp $BOARD_DIR/parameter.txt			$LINUX_UPDATE

pushd linux_update/rockdev
. mkupdate.sh
if [ $? -eq 0 ]; then
        echo "update.img make OK!"
else
        echo "update.img make FAILED!"
        exit 1
fi
popd
chmod 777 linux_update/rockdev/update.img
DATE_CURRENT=$(date +%Y%m%d_%H%M)
mv linux_update/rockdev/update.img $BOARD_DIR/update_"$DATE_CURRENT".img
rm -rf $LINUX_UPDATE/*
