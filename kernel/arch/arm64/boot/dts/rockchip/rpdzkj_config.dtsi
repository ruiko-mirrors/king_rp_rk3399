#include <dt-bindings/input/input.h>

/ {
	
	fiq_debugger: fiq-debugger {
		compatible = "rockchip,fiq-debugger";
		rockchip,serial-id = <2>;
		rockchip,wake-irq = <0>;
		/* If enable uart uses irq instead of fiq */
		rockchip,irq-mode-enable = <1>;
		/* Only 115200 and 1500000 */
		rockchip,baudrate = <115200>;
		pinctrl-names = "default";
		pinctrl-0 = <&uart2c_xfer>;
		interrupts = <GIC_SPI 150 IRQ_TYPE_LEVEL_HIGH 0>;
	};
	
	led-gpio {
		compatible = "led-led";
		breathe-led = <&gpio4 24 GPIO_ACTIVE_HIGH>;
	};
	
	5v_en {
		compatible = "5v-en-gpio";
		power_en-gpio = <&gpio4 30 GPIO_ACTIVE_HIGH>;
		gsm_power_en = <&gpio4 22 GPIO_ACTIVE_HIGH>;
		uart5_en-gpio = <&gpio4 29 GPIO_ACTIVE_HIGH>;
		hub_rst = <&gpio1 24 GPIO_ACTIVE_HIGH>;
		fan_open = <&gpio1 18 GPIO_ACTIVE_HIGH>;
		rp_not_deep_leep = <0>;
		status = "okay";
	};
	
	gpio_ctl{
		compatible = "gpio_ctl";
		base_value = <1000>; //3288_5.1 = 0   3288_7.1.2 = 992  3288_ubuntu = 992   3399_7.1.2 = 1000 3399_ubuntu = 0
		gpio_ctl_num1 = <&gpio4 21 GPIO_ACTIVE_LOW>;

	};

	rockchip-key {
		compatible = "rockchip,key";
		status = "okay";

		io-channels = <&saradc 1>;
		vol-up-key {
			linux,code = <114>;
			label = "volume down";
			rockchip,adc_value = <1>;
		};

		vol-down-key {
			linux,code = <115>;
			label = "volume up";
			rockchip,adc_value = <170>;
		};
		power-key {
			gpios = <&gpio0 5 GPIO_ACTIVE_LOW>;
			linux,code = <116>;
			label = "power";
			gpio-key,wakeup;
		};
	};

	rt5651-sound {
		compatible = "simple-audio-card";
		simple-audio-card,format = "i2s";
		simple-audio-card,name = "realtek,rt5651-codec";
		simple-audio-card,mclk-fs = <256>;
		simple-audio-card,widgets =
			"Microphone", "Mic Jack",
			"Headphone", "Headphone Jack";
		simple-audio-card,routing =
			"Mic Jack", "MICBIAS1",
			"IN1P", "Mic Jack",
			"Headphone Jack", "HPOL",
			"Headphone Jack", "HPOR";
		simple-audio-card,cpu {
			sound-dai = <&i2s0>;
		};
		simple-audio-card,codec {
			sound-dai = <&rt5651>;
		};
	};
	hdmi_sound: hdmi-sound {
//status = "disabled";
		compatible = "simple-audio-card";
		simple-audio-card,format = "i2s";
		simple-audio-card,mclk-fs = <256>;
		simple-audio-card,name = "rockchip,hdmi";

		simple-audio-card,cpu {
			sound-dai = <&i2s2>;
		};
		simple-audio-card,codec {
			sound-dai = <&dw_hdmi_audio>;
		};
	};

	hdmi_codec: hdmi-codec {
		status = "disabled";
		compatible = "simple-audio-card";
		simple-audio-card,format = "i2s";
		simple-audio-card,mclk-fs = <256>;
		simple-audio-card,name = "HDMI-CODEC";

		simple-audio-card,cpu {
			sound-dai = <&i2s2>;
		};

		simple-audio-card,codec {
			sound-dai = <&hdmi>;
		};
	};
	spdif-sound {
		compatible = "simple-audio-card";
		//status = "okay";
		status = "disabled";

		simple-audio-card,name = "ROCKCHIP,SPDIF";
		simple-audio-card,cpu {
			sound-dai = <&spdif>;
		};
		simple-audio-card,codec {
			sound-dai = <&spdif_out>;
		};
	};

	spdif_out: spdif-out {
		compatible = "linux,spdif-dit";
		status = "okay";
		#sound-dai-cells = <0>;
	};

	dw_hdmi_audio: dw-hdmi-audio {
		status = "okay";
		compatible = "rockchip,dw-hdmi-audio";
		#sound-dai-cells = <0>;
	};
	rk_headset {
		compatible = "rockchip_headset";
		headset_gpio = <&gpio4 28 GPIO_ACTIVE_LOW>;
		pinctrl-names = "default";
		pinctrl-0 = <&hp_det>;
//		io-channels = <&saradc 2>;
	};
};

&hdmi {
	#address-cells = <1>;
	#size-cells = <0>;
	#sound-dai-cells = <0>;
//		status = "disabled";
	status = "okay";
};

&i2c1 {
	status = "okay";
	i2c-scl-rising-time-ns = <300>;
	i2c-scl-falling-time-ns = <15>;

	rt5651: rt5651@1a {
		#sound-dai-cells = <0>;
		compatible = "realtek,rt5651";
		reg = <0x1a>;
		clocks = <&cru SCLK_I2S_8CH_OUT>;
		clock-names = "mclk";
		spk-con-gpio = <&gpio0 11 GPIO_ACTIVE_HIGH>;
	};
};

&i2c4 {
	status = "okay";
	i2c-scl-rising-time-ns = <600>;
	i2c-scl-falling-time-ns = <20>;

	max17047@36 {
		status = "okay";
                compatible = "maxim,max17047";
                reg = <0x36>;
//		pinctrl-names = "default";
//                pinctrl-0 = <&BQ24196_gpio>;
                charge-detect-gpio = <&gpio1 9 GPIO_ACTIVE_HIGH>;
                battery-full-detect-gpio = <&gpio1 10 GPIO_ACTIVE_HIGH>;
         };

        sensor@1d {
		status = "okay";
                compatible = "gs_mma8452";
		pinctrl-names = "default";
		pinctrl-0 = <&mma8452_irq_gpio>;
                reg = <0x1d>;
                type = <2>;
                irq-gpio = <&gpio1 23 IRQ_TYPE_EDGE_FALLING>;
                irq_enable = <1>;
                poll_delay_ms = <30>;
//                layout = <2>;
                layout = <4>;
        };
};
&i2s0 {
	status = "okay";
	rockchip,i2s-broken-burst-len;
	rockchip,playback-channels = <8>;
	rockchip,capture-channels = <8>;
	#sound-dai-cells = <0>;
};
&i2s2 {
	#sound-dai-cells = <0>;
	status = "okay";
};

&pinctrl {
	headphone {
		hp_det: hp-det {
			rockchip,pins = <4 28 RK_FUNC_GPIO &pcfg_pull_up>;
		};
	};
	mma8452 {
		mma8452_irq_gpio: mma8452-irq-gpio {
			rockchip,pins = <1 23 RK_FUNC_GPIO &pcfg_pull_up>;
		};
	};

//        BQ24196_gpio {
//                BQ24196_gpio: BQ24196_gpio {
//                        rockchip,pins =
//                                <1 9 RK_FUNC_GPIO &pcfg_pull_up>,
//                                <1 10 RK_FUNC_GPIO &pcfg_pull_up>;
//                };
//        };
};

&pwm2 {
	status = "okay";
};

//temperature
//ctl open
&threshold {
	temperature = <75000>;
};
//ctl max
&target {
	temperature = <100000>;
};
//ctl soft power off
&soc_crit {
	temperature = <105000>;
};
&tsadc {
	// tshut mode 0:CRU 1:GPIO 
	rockchip,hw-tshut-mode = <1>;
	// tshut polarity 0:LOW 1:HIGH 
	rockchip,hw-tshut-polarity = <1>;
	rockchip,hw-tshut-temp = <110000>;
	status = "okay";
};
&uart0 {
	pinctrl-names = "default";
	pinctrl-0 = <&uart0_xfer &uart0_cts>;
	status = "okay";
};

&uart2 {
	status = "okay";
};
&uart4 {
	status = "okay";
};

&pwm3 {
//	status = "okay";	
	status = "disabled";
	interrupts = <GIC_SPI 61 IRQ_TYPE_LEVEL_HIGH 0>;
	compatible = "rockchip,remotectl-pwm";
	remote_pwm_id = <3>;
	handle_cpu_id = <0>;

    ir_key1{
        rockchip,usercode = <0xff00>;
        rockchip,key_table =
            <0xeb   KEY_POWER>,
            <0xec   KEY_MENU>,
            <0xfe   KEY_BACK>,
            <0xb7   KEY_HOME>,
            <0xa3   KEY_WWW>,
            <0xf4   KEY_VOLUMEUP>,
            <0xa7   KEY_VOLUMEDOWN>,
            <0xf8   KEY_REPLY>,
            <0xfc   KEY_UP>,
            <0xfd   KEY_DOWN>,
            <0xf1   KEY_LEFT>,
            <0xe5   KEY_RIGHT>;
    };
};
